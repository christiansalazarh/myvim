" Vimrc file.  Christian Salazar. <christiansalazarh@gmail.com> 
colorscheme christian
" colorscheme elflord
" colorscheme solarized8_dark_high
set nocompatible
syntax on

" para ver los scripts cargados por vim:
" 
" :scriptnames

" presionar: \cpf en el archivo php abierto
" source ~/.vim/plugin/phpcsfixer.vim

" presionar: control+p en una clase o un metodo
" source ~/.vim/plugin/phpdoc.vim
"let g:pdv_cfg_Author = "Christian Salazar <hola@2amigos.us>"
"let g:pdv_cfg_Company = "2amigOS! <https://2amigos.us>"
let g:pdv_cfg_Author = "Cristian Salazar <proyectos@chileshift.cl>"
"let g:pdv_cfg_Company = "2amigOS! <https://2amigos.us>"

" presionar: \l para mostrar caracteres ocultos 
nmap <leader>l :set list!<CR>

" Use the same symbols as TextMate for tabstops and EOLs
set listchars=tab:▸\ ,eol:¬

inoremap <F3> <ESC>:Exp<CR>
nnoremap <F3> <ESC>:Exp<CR>

inoremap <F4> <ESC>:w<CR>:bd<CR>
nnoremap <F4> <ESC>:w<CR>:bd<CR>

inoremap <F12> :tabnew<CR>:e /home/christian/.vimrc<CR>
nnoremap <F12> :tabnew<CR>:e /home/christian/.vimrc<CR>

inoremap <F11> :tabnew<CR>:e /home/christian/.vim/colors/christian.vim<CR>
nnoremap <F11> :tabnew<CR>:e /home/christian/.vim/colors/christian.vim<CR>

inoremap <F10> :tabnew<CR>:e /home/christian/.vim/syntax/miphp.vim<CR>
nnoremap <F10> :tabnew<CR>:e /home/christian/.vim/syntax/miphp.vim<CR>

"F8 guarda y vuelve a insertmode
inoremap <F8> <ESC>:w<cr>
nnoremap <F8> <ESC>:w<cr>

"releer (deberia usarse sobre un archivo .vimrc)
ino2remap <F9> <esc>:so %<cr>i
nnoremap <F9> <esc>:so %<cr>i

" para tab block multi lines:
" Pressing < or > will let you indent/unident selected lines
vnoremap < <gv
vnoremap > >gv

" mueve atras y adelante en lista de buffers de forma ciclica
:noremap <F6> <ESC>:bn<CR>
:noremap <F5> <ESC>:bp<CR>
:inoremap <F6> <ESC>:bn<CR>
:inoremap <F5> <ESC>:bp<CR>


" CTRL-Z is Undo; not in cmdline though
noremap <C-Z> u
inoremap <C-Z> <C-O>u
" CTRL-Y is Redo (although not repeat); not in cmdline though
" noremap <C-Y> <C-R>
" inoremap <C-Y> <C-O><C-R>                                    

"la mierdita de culebra que nunca se donde esta.
"noremap <M> ~

" CTRL-A is Select all
noremap <C-A> gggH<C-O>G
inoremap <C-A> <C-O>gg<C-O>gH<C-O>G
cnoremap <C-A> <C-C>gggH<C-O>G
onoremap <C-A> <C-C>gggH<C-O>G
snoremap <C-A> <C-C>gggH<C-O>G
xnoremap <C-A> <C-C>ggVG

"la marca de color del borde derecho y el color del texto fuera de ancho
highlight OverLength guibg=#00ff00
match OverLength /\%80v.\+/
if exists('+colorcolumn')
  set colorcolumn=80
endif

" PHP documenter script bound to Control-P
autocmd FileType php inoremap <C-p> <ESC>:call PhpDocSingle()<CR>i
autocmd FileType php nnoremap <C-p> :call PhpDocSingle()<CR>
autocmd FileType php vnoremap <C-p> :call PhpDocRange()<CR>

" habilita el autocomplete de lenguaje CSS
" uso: mientras se edita un archivo CSS, presionar: control+x control+o
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

" otros
"set backupdir=/tmp
set directory=$HOME/.swapfiles//

set lazyredraw
set ls=1            " allways show status line

set tabstop=4       " numbers of spaces of tab character
set shiftwidth=4    " numbers of spaces to (auto)indent
set expandtab      " tabs are converted to spaces, use only when required
"set list
"set listchars=tab:>-
set softtabstop=4 " makes the spaces feel like real tabs
"set mouse=a
set scrolloff=10     " keep 3 lines when scrolling
set hlsearch        " highlight searches
set incsearch       " do incremental searching
set ruler           " show the cursor position all the time
set visualbell t_vb=    " turn off error beep/flash
"set novisualbell    " turn off visual bell
set nobackup        " do not keep a backup file
set number          " show line numbers
set ignorecase      " ignore case when searching
set title           " show title in console title bar
"set ttyfast         " smoother changes
set ttyscroll=0        " turn off scrolling, didn't work well with PuTTY
set modeline        " last lines in document sets vim mode
set modelines=3     " number lines checked for modelines
set shortmess=atI   " Abbreviate messages
set nostartofline   " don't jump to first character when paging
set whichwrap=b,s,h,l,<,>,[,]   " move freely between files
set viminfo='20,<50,s10,h
set autoindent     " always set autoindenting on
set smartindent        " smart indent
"set cindent            " cindent
"set noautoindent
"set nosmartindent
"set nocindent  
"set autowrite      " auto saves changes when quitting and swiching buffer
set sm             " show matching braces, somewhat annoying...
set nowrap         " don't wrap lines

   if has("autocmd")
       " When using mutt, text width=72
       au FileType cpp,c,java,sh,pl,php,asp  set autoindent
       au FileType cpp,c,java,sh,pl,php,asp  set smartindent
       au FileType cpp,c,java,sh,pl,php,asp  set cindent
       "au BufRead mutt*[0-9] set tw=72
   endif


"fin de archivo
